﻿using System.Collections.Generic;

namespace MagicBox.PayDayLoanInquiryReport
{
    public class LoanInquiryPDADetail
    {
        public LoanInquiryPDADetail()
        {
            LoanInquiryPDADetailInqs = new List<LoanInquiryPDADetailInq>();
            LoanInquiryPDADetailLoans = new List<LoanInquiryPDADetailLoan>();
        }

        public List<LoanInquiryPDADetailInq> LoanInquiryPDADetailInqs { get; set; }
        public List<LoanInquiryPDADetailLoan> LoanInquiryPDADetailLoans { get; set; }
    }
}
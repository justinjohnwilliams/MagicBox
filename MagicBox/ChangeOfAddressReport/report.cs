﻿using System.Xml.Serialization;

namespace MagicBox.ChangeOfAddressReport
{
    public class report
    {
        [XmlElement("record")]
        public record[] Records { get; set; }
    }
}
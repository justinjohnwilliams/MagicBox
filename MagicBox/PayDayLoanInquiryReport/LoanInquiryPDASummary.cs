﻿namespace MagicBox.PayDayLoanInquiryReport
{
    public class LoanInquiryPDASummary
    {
        public string inquiries { get; set; }
        public string recentinquiries { get; set; }
        public string merchants { get; set; }
        public string recentmerchants { get; set; }
        public string lastinquirydate { get; set; }
        public string badloans { get; set; }
        public string badloanmerchants { get; set; }
        public string loanscurrent { get; set; }
        public string loanspastdue { get; set; }
        public string loanswrittenoff { get; set; }
        public string loansdischarged { get; set; }
        public string badloandate { get; set; }
        public string loanscollections { get; set; }
        public string loans { get; set; }
        public string lastloandate { get; set; }
        public string fdicloandays { get; set; }
    }
}
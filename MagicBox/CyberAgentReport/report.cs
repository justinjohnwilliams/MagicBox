﻿using System.Xml.Serialization;

namespace MagicBox.CyberAgentReport
{
    public class report
    {
        [XmlElement("record")]
        public record[] Records { get; set; }
    }
}
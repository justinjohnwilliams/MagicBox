﻿using System.Xml.Serialization;

namespace MagicBox.PayDayLoanInquiryReport
{
    public class inquiry
    {
        [XmlElement("ssn")]
        public string ssn { get; set; }
    }
}
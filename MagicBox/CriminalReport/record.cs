﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MagicBox.CriminalReport
{
    public class record
    {
        public string Fines { get; set; }
        public string Age { get; set; }
        public string FirstName { get; set; }
        public string NCICCode { get; set; }
        public string caseType { get; set; }
        public string Plea { get; set; }
        public string Sex { get; set; }
        public string Generation { get; set; }
        public string DispositionDate { get; set; }
        public string MiddleName { get; set; }
        public string Source { get; set; }
        public string ConvictionDate { get; set; }
        public string PhotoName { get; set; }
        public string OffenseDesc1 { get; set; }
        public string sourceName { get; set; }
        public string Height { get; set; }
        public string ArrestingAgency { get; set; }
        public string ConvictionPlace { get; set; }
        public string Hair { get; set; }
        public string Address1 { get; set; }
        public string DOB { get; set; }
        public string ChargesFiledDate { get; set; }
        public string SkinTone { get; set; }
        public string SentenceYYYMMDDD { get; set; }
        public string ScarsMarks { get; set; }
        public string sourceState { get; set; }
        public string sha1 { get; set; }
        public string Address2 { get; set; }
        public string OffenseDesc2 { get; set; }
        public string DOBAKA { get; set; }
        public string City { get; set; }
        public string Eye { get; set; }
        public string State { get; set; }
        public string MilitaryService { get; set; }
        public string Disposition { get; set; }
        public string LastName { get; set; }
        public string Zip { get; set; }
        public string Counts { get; set; }
        public string ProbationYYYMMDDD { get; set; }
        public string OffenseCode { get; set; }
        public string Race { get; set; }
        public string OffenseDate { get; set; }
        public string Weight { get; set; }
        public string AKA2 { get; set; }
        public string BirthState { get; set; }
        public string AKA1 { get; set; }
        public string CourtCosts { get; set; }
        public string Court { get; set; }
        public string IDCaseNumber { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            var serializer = new XmlSerializer(GetType());
            using (var sww = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sww))
                {
                    serializer.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }
    }
}
﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MagicBox.ChangeOfAddressReport
{
    public class record
    {
        public string aid { get; set; }
        public string sid { get; set; }
        public string gid { get; set; }
        public string first { get; set; }
        public string middle { get; set; }
        public string last { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string zip { get; set; }
        public string m { get; set; }
        public string moveda { get; set; }
        public string ma { get; set; }
        public string nxi_ { get; set; }
        public string an { get; set; }

        public override string ToString()
        {
            var serializer = new XmlSerializer(GetType());
            using (var sww = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sww))
                {
                    serializer.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }
    }
}
﻿namespace MagicBox.PayDayLoanInquiryReport
{
    public class LoanInquiryPDADetailLoan
    {
        public virtual string date { get; set; }
        public virtual string amount { get; set; }
        public virtual string status { get; set; }
        public virtual string merchant { get; set; }
        public virtual string citystatezip { get; set; }
        public virtual string phone { get; set; }
    }
}
﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MagicBox.NameAliasAddressHistoryReport
{
    public class transaction
    {
        [XmlElement("data")]
        public data[] Data { get; set; }

        [XmlElement("status")]
        public status Status { get; set; }

        public override string ToString()
        {
            var serializer = new XmlSerializer(GetType());
            using (var sww = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sww))
                {
                    serializer.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }
    }
}
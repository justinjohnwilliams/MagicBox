﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MagicBox.Common;

namespace MagicBox.PayDayLoanInquiryReport
{
    [XmlRoot("clv-response")]
    public class report
    {
        private string _xml;

        public report(string xml)
        {
            _xml = xml;
            Address = new MailingAddress();
            LoanInquiryAAItems = new List<LoanInquiryAAItem>();
            LoanInquiryPDAs = new List<LoanInquiryPDA>();
        }

        public string Ssn { get; set; }
        public string TranId { get; set; }
        public string TranLinkId { get; set; }
        public string TranDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MailingAddress Address { get; set; }
        public List<LoanInquiryAAItem> LoanInquiryAAItems { get; set; }
        public List<LoanInquiryPDA> LoanInquiryPDAs { get; set; }

        public override string ToString()
        {
            return _xml;
        }
    }
}
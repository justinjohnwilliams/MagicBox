﻿using System.Xml.Serialization;

namespace MagicBox.CriminalReport
{
    public class report
    {
        [XmlElement("record")]
        public record[] Records { get; set; }
    }
}
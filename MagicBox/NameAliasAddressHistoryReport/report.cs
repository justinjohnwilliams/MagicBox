﻿using System.Xml.Serialization;

namespace MagicBox.NameAliasAddressHistoryReport
{
    public class report
    {
        [XmlElement("transaction")]
        public transaction Transaction { get; set; }
    }
}
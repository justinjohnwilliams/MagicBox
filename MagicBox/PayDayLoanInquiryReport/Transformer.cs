﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MagicBox.Common;

namespace MagicBox.PayDayLoanInquiryReport
{
    public static class Transformer
    {
        public static report Transform(string xml)
        {
            var result = new report(xml);

            var doc = XDocument.Parse(xml);

            if (doc.Document == null || doc.Document.Root == null ||
                !doc.Document.Root.Elements("inquiry").Elements("pda").Elements("detail").Any())
            {
                return result;
            }

            foreach (var recordElement in doc.Document.Root.Elements("inquiry"))
            {
                result.Ssn = recordElement.GetElementValue("ssn");
                result.TranId = recordElement.GetElementValue("tran-id");
                result.TranLinkId = recordElement.GetElementValue("tran-link-id");
                result.TranDate = recordElement.GetElementValue("tran-date");
                result.LastName = recordElement.GetElementValue("last-name");
                result.FirstName = recordElement.GetElementValue("first-name");
                if (recordElement.Element("address") != null)
                {
                    result.Address.Line1 = recordElement.Element("address").GetElementValue("street");
                    result.Address.City = recordElement.Element("address").GetElementValue("city");
                    result.Address.State = recordElement.Element("address").GetElementValue("state");
                    result.Address.ZipCode = recordElement.Element("address").GetElementValue("zip");
                }

                foreach (var aaItem in recordElement.Elements("aa-item"))
                {
                    result.LoanInquiryAAItems.Add(new LoanInquiryAAItem
                    {
                        Code = aaItem.GetAttributeValue("code"),
                        Description = aaItem.GetAttributeValue("desc")
                    });
                }

                foreach (var pdaElement in recordElement.Elements("pda"))
                {
                    result.LoanInquiryPDAs.Add(new LoanInquiryPDA
                    {
                        Validity = pdaElement.GetElementValue("validity"),
                        LoanInquiryPDASummaries = ReadPDASummaries(pdaElement),
                        LoanInquiryPDADetails = ReadLoanInquiryPDADetails(pdaElement)
                    });
                }
            }

            return result;
        }

        public static List<LoanInquiryPDASummary> ReadPDASummaries(XElement pdaElement)
        {
            return pdaElement.Elements("summary")
                .Select(summaryElement =>
                    new LoanInquiryPDASummary
                    {
                        inquiries = summaryElement.GetElementValue("inquiries"),
                        recentinquiries = summaryElement.GetElementValue("recent-inquiries"),
                        merchants = summaryElement.GetElementValue("merchants"),
                        recentmerchants = summaryElement.GetElementValue("recent-merchants"),
                        lastinquirydate = summaryElement.GetElementValue("last-inquiry-date"),
                        badloans = summaryElement.GetElementValue("bad-loans"),
                        badloanmerchants = summaryElement.GetElementValue("bad-loan-merchants"),
                        loanscurrent = summaryElement.GetElementValue("loans-current"),
                        loanspastdue = summaryElement.GetElementValue("loans-past-due"),
                        loanswrittenoff = summaryElement.GetElementValue("loans-written-off"),
                        loansdischarged = summaryElement.GetElementValue("loans-discharged"),
                        badloandate = summaryElement.GetElementValue("bad-loan-date"),
                        loanscollections = summaryElement.GetElementValue("loans-collections"),
                        loans = summaryElement.GetElementValue("loans"),
                        lastloandate = summaryElement.GetElementValue("last-loan-date"),
                        fdicloandays = summaryElement.GetElementValue("fdic-loan-days")
                    }).ToList();
        }

        public static List<LoanInquiryPDADetail> ReadLoanInquiryPDADetails(XElement pdaElement)
        {
            return pdaElement.Elements("detail").Select(detail => new LoanInquiryPDADetail
            {
                LoanInquiryPDADetailInqs = ReadLoanInquiryPDADetailInqs(detail),
                LoanInquiryPDADetailLoans = ReadLoanInquiryPDADetailLoan(detail)
            }).ToList();
        }

        public static List<LoanInquiryPDADetailInq> ReadLoanInquiryPDADetailInqs(XElement detailElement)
        {
            return detailElement.Elements("inq").Select(inq => new LoanInquiryPDADetailInq
            {
                date = inq.GetAttributeValue("date"),
                merchant = inq.GetAttributeValue("merchant"),
                citystatezip = inq.GetAttributeValue("city-state-zip"),
                phone = inq.GetAttributeValue("phone"),
                time = inq.GetAttributeValue("time")
            }).ToList();
        }

        public static List<LoanInquiryPDADetailLoan> ReadLoanInquiryPDADetailLoan(XElement detailElement)
        {
            return detailElement.Elements("loan").Select(loan => new LoanInquiryPDADetailLoan
            {
                date = loan.GetAttributeValue("date"),
                amount = loan.GetAttributeValue("amount"),
                status = loan.GetAttributeValue("status"),
                merchant = loan.GetAttributeValue("merchant"),
                citystatezip = loan.GetAttributeValue("city-state-zip"),
                phone = loan.GetAttributeValue("phone")
            }).ToList();
        }
    }
}
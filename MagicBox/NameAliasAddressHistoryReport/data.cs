﻿using System;

namespace MagicBox.NameAliasAddressHistoryReport
{
    public class data
    {
        public string first { get; set; }
        public string last { get; set; }
        public string phone { get; set; }
        public string dob { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string source { get; set; }
        public string date { get; set; }
        public string rank { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
﻿using System.Xml.Linq;

namespace MagicBox.Common
{
    public static class extensions
    {
        public static string GetElementValue(this XElement element, string valueName)
        {
            var xElement = element.Element(valueName);
            return xElement?.Value ?? "";
        }

        public static string GetAttributeValue(this XElement element, string attributeName)
        {
            var xElement = element.Attribute(attributeName);
            return xElement?.Value ?? "";
        }

    }
}
﻿using System;
using System.IO;
using SexOffenderReport_519 = MagicBox.SexOffenderReport.report;
using ChangeOfAddressReport_524 = MagicBox.ChangeOfAddressReport.report;
using CriminalReport_502 = MagicBox.CriminalReport.report;
using CyberAgentReport_503 = MagicBox.CyberAgentReport.report;
using NameAliasAddressHistoryReport_518 = MagicBox.NameAliasAddressHistoryReport.report;
using PayDayLoanInquiryReport_505 = MagicBox.PayDayLoanInquiryReport.report;

namespace MagicBox
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var sexOffenderReport = ParseSexOffenderReport();
            var changeOfAddressReport = ParseChangeOfAddressReport();
            var criminalReport = ParseCriminalReport();
            var cyberAgentReport = ParseCyberAgentReport();
            var nameAliasAddressHistoryReport = ParseNameAliasAddressHistoryReport();
            var payDayLoanInquiryReport = ParsePayDayLoanInquiryReport();

            Console.ReadLine();
        }

        public static SexOffenderReport_519 ParseSexOffenderReport()
        {
            string xml;
            const string sexOffenderReport_519 = "519_SexOffenderReport.xml";
            using (var sr = new StreamReader("_resources/" + sexOffenderReport_519))
            {
                xml = sr.ReadToEnd();
            }
            return SexOffenderReport.Transformer.Transform(xml);
        }

        public static ChangeOfAddressReport_524 ParseChangeOfAddressReport()
        {
            string xml;
            const string changeOfAddressReport_524 = "524_ChangeOfAddressReport.xml";
            using (var sr = new StreamReader("_resources/" + changeOfAddressReport_524))
            {
                xml = sr.ReadToEnd();
            }
            return ChangeOfAddressReport.Transformer.Transform(xml);
        }

        public static CriminalReport_502 ParseCriminalReport()
        {
            string xml;
            const string criminalReport_502 = "502_CriminalReport.xml";
            using (var sr = new StreamReader("_resources/" + criminalReport_502))
            {
                xml = sr.ReadToEnd();
            }
            return CriminalReport.Transformer.Transform(xml);
        }

        public static CyberAgentReport_503 ParseCyberAgentReport()
        {
            string xml;
            const string cyberReport_503 = "503_CyberReport.xml";
            using (var sr = new StreamReader("_resources/" + cyberReport_503))
            {
                xml = sr.ReadToEnd();
            }
            return CyberAgentReport.Transformer.Transform(xml);
        }

        public static NameAliasAddressHistoryReport_518 ParseNameAliasAddressHistoryReport()
        {
            string xml;
            const string addressHistoryReport_518 = "518_AddressHistoryReport.xml";
            using (var sr = new StreamReader("_resources/" + addressHistoryReport_518))
            {
                xml = sr.ReadToEnd();
            }
            return NameAliasAddressHistoryReport.Transformer.Transform(xml);
        }

        public static PayDayLoanInquiryReport_505 ParsePayDayLoanInquiryReport()
        {
            string xml;

            const string nonCreditReport_505 = "505_NonCreditReport.xml";


            using (var sr = new StreamReader("_resources/" + nonCreditReport_505))
            {
                xml = sr.ReadToEnd();
            }

            return PayDayLoanInquiryReport.Transformer.Transform(xml);
        }
    }
}
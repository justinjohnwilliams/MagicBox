﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace MagicBox.ChangeOfAddressReport
{
    public static class Transformer
    {
        public static report Transform(string xml)
        {
            report result;
            var serializer = new XmlSerializer(typeof (report));
            var buffer = Encoding.UTF8.GetBytes(xml);
            using (var stream = new MemoryStream(buffer))
            {
                result = (report) serializer.Deserialize(stream);
            }

            return result;
        }
    }
}
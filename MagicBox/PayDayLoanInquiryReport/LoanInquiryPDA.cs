﻿using System.Collections.Generic;

namespace MagicBox.PayDayLoanInquiryReport
{
    public class LoanInquiryPDA
    {
        public LoanInquiryPDA()
        {
            LoanInquiryPDASummaries = new List<LoanInquiryPDASummary>();
            LoanInquiryPDADetails = new List<LoanInquiryPDADetail>();
        }

        public string Validity { get; set; }
        public List<LoanInquiryPDASummary> LoanInquiryPDASummaries { get; set; }
        public List<LoanInquiryPDADetail> LoanInquiryPDADetails { get; set; }
    }
}
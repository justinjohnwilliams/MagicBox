﻿namespace MagicBox.PayDayLoanInquiryReport
{
    public class LoanInquiryPDADetailInq
    {
        public string date { get; set; }
        public string merchant { get; set; }
        public string citystatezip { get; set; }
        public string phone { get; set; }
        public string time { get; set; }
    }
}
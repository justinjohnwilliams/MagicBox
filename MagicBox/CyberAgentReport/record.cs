﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MagicBox.CyberAgentReport
{
    public class record
    {
        private string _cardcvn;
        private string _cardnum;
        private string _ssn;
        public string dobyear { get; set; }
        public string addrstate { get; set; }
        public string bankroute { get; set; }
        public string ebaypwd { get; set; }
        public string email { get; set; }
        public string addrcountry { get; set; }
        public string password { get; set; }
        public string dobmon { get; set; }
        public string CreationDate { get; set; }
        public string phoneext { get; set; }
        public string Source { get; set; }
        public string ebayuid { get; set; }

        [XmlElement("id")]
        public string AgentId { get; set; }

        public string cardexpyear { get; set; }
        public string cardpin { get; set; }
        public string bankpin { get; set; }


        public string dlstate { get; set; }
        public string bankpwd { get; set; }
        public string bankuid { get; set; }
        public string addrcity { get; set; }
        public string addrzip { get; set; }

        public string cardcvn
        {
            get { return _cardcvn; }
            set
            {
                _cardcvn = value;
                if (!string.IsNullOrEmpty(_cardcvn)) cvnFound = true;
            }
        }

        public bool cvnFound { get; set; }

        public string ssn
        {
            get { return _ssn; }
            set
            {
                _ssn = value;
                if (!string.IsNullOrEmpty(_ssn)) ssnFound = true;
            }
        }

        public bool ssnFound { get; set; }
        public string dob { get; set; }
        public string mmn { get; set; }
        public string phone1 { get; set; }
        public string paypalpwd { get; set; }
        public string phone2 { get; set; }
        public string sha1 { get; set; }
        public string bankname { get; set; }
        public string userid { get; set; }
        public string cardexpday { get; set; }
        public string addr2 { get; set; }
        public string namelast { get; set; }
        public string bankphone { get; set; }
        public string cardtype { get; set; }

        public string cardnum
        {
            get { return _cardnum; }
            set { _cardnum = value != null ? value.Length > 4 ? value.Substring(value.Length - 4) : value : value; }
        }

        public string addr1 { get; set; }
        public string namemid { get; set; }
        public string dobday { get; set; }
        public string phone3 { get; set; }
        public string SourceDate { get; set; }
        public string bankacct { get; set; }
        public string phone { get; set; }
        public string note { get; set; }
        public string paypaluid { get; set; }
        public string cardexpdate { get; set; }
        public string cardexpmon { get; set; }
        public string cardname { get; set; }
        public string namefirst { get; set; }
        public string cardaddr { get; set; }
        public string UpdateDate { get; set; }

        public override string ToString()
        {
            var serializer = new XmlSerializer(GetType());
            using (var sww = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sww))
                {
                    serializer.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }
    }
}